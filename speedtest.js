<script src="blake2s.js" type="text/javascript"></script>

<script type="text/javascript">
var data = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
var n = 10000;

console.time();
for (var i = 0; i < n; ++i) {
	var hash = blake2s(data + n).toString();
}
console.timeEnd();

</script>

<script src="altblake.js" type="text/javascript"></script>

<script type="text/javascript">

console.time();
for (var i = 0; i < n; ++i) {
	var h = new BLAKE2s(32);
	h.update(new Uint8Array(data + n));
	var hash = h.hexDigest(); 
}
console.timeEnd();

</script>
