/********************
 * Finding elements *
 ********************/

/**********
 * Events *
 **********/

function matAttachEvent(el, ev, f) {
	if (el._mat_events == undefined)
		el._mat_events = {};
	if (ev == "wheel") {
		if (el._mat_events.wheel == undefined)
			el._mat_events.wheel = [];
		el._mat_events.wheel.push(f);
		if (el.addEventListener) {
			// IE9, Chrome, Safari, Opera
			el.addEventListener("mousewheel", _mat_event_wheel, false);
			// Firefox
			el.addEventListener("DOMMouseScroll", _mat_event_wheel, false);
		} else {
			// IE 6/7/8
			el.attachEvent("onmousewheel" + ev, _mat_event_wheel);
		}
	}
}

function matDetachEvent(el, ev, f) {
	try {
		el._mat_events.wheel.indexOf(f);
		if (el.addEventListener) {
			// IE9, Chrome, Safari, Opera
			el.removeEventListener(ev, f);
			if (ev == "mousewheel") {
				el.removeEventListener("DOMMouseScroll", f);
			}
		} else {
			// IE 6/7/8
			el.detachEvent("on" + ev, f);
		}
	}
}

function matPreventDefault(ev) {
	ev.ev.preventDefault ? ev.ev.preventDefault() : (ev.ev.returnValue = false);
}

function _mat_event_wheel(e) {
	for (var i = 0; i < this._mat_events.wheel.length; ++i) {
		this._mat_events.wheel[i]({
			ev: window.event || e,
			delta: Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))
		});
	}
}
