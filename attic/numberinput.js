var inputs = document.getElementsByTagName("input");

for (var i = 0; i < inputs.length; ++i) {
	if (inputs[i].getAttribute("type") != "number")
		continue;
	var e = inputs[i];
	if (e.addEventListener) {
		// IE9, Chrome, Safari, Opera
		e.addEventListener("mousewheel", wheel, false);
		// Firefox
		e.addEventListener("DOMMouseScroll", wheel, false);
	}
	// IE 6/7/8
	else e.attachEvent("onmousewheel", wheel);
}

function wheel(e) {
	var e = window.event || e; // old IE support
	e.preventDefault ? e.preventDefault() : (e.returnValue = false);
	var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
	if (delta > 0)
		this.value++;
	else this.value--;
}
