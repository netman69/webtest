function mQuerySelector(q, el) {
	if (el == undefined)
		el = document;
	return el.querySelectorAll(q);
}

function mTrim(s) {
	return s.trim(s);
}

if (String.prototype.trim == undefined) {
	mTrim = function(s) {
		s = s.replace(/^\s+/, "");
		s = s.replace(/\s+$/, "");
		return s;
	}
}

if (document.querySelectorAll == undefined) {
	var mQuerySelectorStyle = document.createElement('style');

	mQuerySelector = function(q, el) {
		if (el == undefined)
			el = document;
		var els = [];
		var all = el.getElementsByTagName("*");
		mQuerySelectorStyle.cssText = q + "{foo:bar}";
		for (var i = 0; i < all.length; ++i) {
			if (getComputedStyle(all[i], null).foo === "bar") {
				els.push(all[i]);
			}
		}
		mQuerySelectorStyle.cssText = "";
		return els;
	}

	//var mQuerySelectorStyle = document.createStyleSheet();
	/* TODO split query by commas, trim. */
	/*mQuerySelector = function(q, el) {
		if (el == undefined)
			el = document;
		var els = [];
		var all = el.all;
		mQuerySelectorStyle.addRule(q, "foo:bar");
		for (var i = 0; i < all.length; ++i) {
			if (all[i].currentStyle.foo === "bar") {
				els.push(all[i]);
			}
		}
		mQuerySelectorStyle.removeRule(0);
		return els;
	}*/
}

function $(q, el) {
	var els = mQuerySelector(q, el);
	els.each = function(f) {
		for (var i = 0; i < els.length; ++i)
			f(els[i]);
	};
	return els;
}

var inputs = $("input", $("#abc")[0]).each(function(e) {
	alert(e.type);
});
