//placeholders(document);

function placeholders(e) {
	var inputs = e.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; ++i) {
		var ph = inputs[i].getAttribute("placeholder");
		if (ph == "" || ph == undefined)
			continue;
		placeholder(inputs[i]);
	}
}

function placeholder(e) {
	var ph = e.getAttribute("placeholder");
	e.setAttribute("placeholder", "");
	e.mPlaceholder = e.cloneNode(false);
	e.mPlaceholder.value = ph;
	e.mPlaceholder.onfocus = placeholder_off;
	e.mPlaceholder.mPlaceholder = e;
	e.mPlaceholder.className += " placeholder";
	e.onblur = placeholder_on;

	e.mIsEmpty = (e.value == undefined || e.value == "");
	if (e.mIsEmpty)
		e.parentNode.replaceChild(e.mPlaceholder, e);

	function placeholder_on() {
		this.mIsEmpty = (this.value == undefined || this.value == "");
		if (this.mIsEmpty)
			this.parentNode.replaceChild(this.mPlaceholder, this);
	}

	function placeholder_off() {
		this.mPlaceholder.mIsEmpty = (this.mPlaceholder.value == undefined || this.mPlaceholder.value == "");
		if (this.mPlaceholder.mIsEmpty)
			this.parentNode.replaceChild(this.mPlaceholder, this);
		this.mPlaceholder.focus();
	}
}
