/* blake2s()
 *
 * Blake2s hash function in javascript using ByteArray class.
 *
 * Arguments:
 *   msg: The message to be hashed.
 *     Both can be a string or an array of bytes, strings and byte arrays (they'll be concatenated).
 *   key: Optional key.
 *   outlen: Output length, 32 if unspecified.
 *
 * Return value:
 *   ByteArray, .toString() or .toString("hex") will make them into hex hash.
 *     For base64 or base64url encoding you can use .toString("base64") and .toString("base64url") respectively.
 *     The base64url format comes without trailing "=".
 *   Also the arrays will have .ints, which is an array storing the hash as 8 32-bit integers.
 *
 */
function blake2s(msg, key, outlen) {
	/* Context */
	var ctx_buf = []; /* uint8[64] - buffer */
	var ctx_bufpos; /* uint8 - position in buffer */
	var ctx_state = []; /* uint32[8] - state */
	var ctx_posl; /* uint32 - position low dword */
	var ctx_posh; /* uint32 - position high dword */

	/* Static stuff */
	var initv = [ /* uint32 */
		0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
		0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
	];

	var sigma = [ /* uint8 */
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
		14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3,
		11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4,
		7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8,
		9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13,
		2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9,
		12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11,
		13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10,
		6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5,
		10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0
	];

	var map = [ /* uint8 */
		0, 4,  8, 12,
		1, 5,  9, 13,
		2, 6, 10, 14,
		3, 7, 11, 15,
		0, 5, 10, 15,
		1, 6, 11, 12,
		2, 7,  8, 13,
		3, 4,  9, 14
	];

	/* Compression function */
	function compress() {
		var i, a, b, c, d;
		var v = ctx_state.concat(initv);
		var buf = [];

		for (i = 0; i < 64; i += 4) /* Translate buffer to dwords */
			buf.push((ctx_buf[i + 0] & 0xFF) + ((ctx_buf[i + 1] & 0xFF) << 8) + ((ctx_buf[i + 2] & 0xFF) << 16) + ((ctx_buf[i + 3] & 0xFF) << 24));

		v[12] ^= ctx_posl;
		v[13] ^= ctx_posh;
		if (ctx_bufpos != 0) /* Last block if non-0 */
			v[14] = ~v[14];

		i = 0;
		while (i < 160) {
			d = (i & 14) << 1; /* d is temporary storage before setting it */
			a = map[d++];
			b = map[d++];
			c = map[d++];
			d = map[d];
			v[a] = v[a] + v[b] + buf[sigma[i++]];
			v[d] = ror(v[d] ^ v[a], 16);
			v[c] = v[c] + v[d];
			v[b] = ror(v[b] ^ v[c], 12);
			v[a] = v[a] + v[b] + buf[sigma[i++]];
			v[d] = ror(v[d] ^ v[a], 8);
			v[c] = v[c] + v[d];
			v[b] = ror(v[b] ^ v[c], 7);
		}

		for(i = 0; i < 8; ++i)
			ctx_state[i] ^= v[i] ^ v[i + 8];
	}

	/* Needed by the compress function */
	function ror(a, b) { return (a >>> b) | (a << 32 - b); }

	/* Initialize context */
	function init(key, outlen) {
		var i;

		ctx_state = initv.slice();
		ctx_state[0] ^= 0x01010000 ^ (key.length << 8) ^ outlen;

		ctx_posl = 0;
		ctx_posh = 0;
		ctx_bufpos = 0;

		if (key.length > 0) {
			for (i = key.length; i < 64; ++i) /* Pad key with zeros */
				ctx_buf[i] = 0;
			update(key);
			ctx_bufpos = 64; /* So key counts as one block */
		}
	}

	/* Update hash */
	function update(input) {
		var i;

		for (i = 0; i < input.length; ++i) {
			if (ctx_bufpos == 64) {
				ctx_posl = (ctx_posl + 64) & 0xFFFFFFFF;
				if (ctx_posl < 64)
					ctx_posh = (ctx_posh + 1) & 0xFFFFFFFF;
				ctx_bufpos = 0; /* We need to zero this anyway, but it also signals to compress() that this isn't the last block */
				compress();
			}
			ctx_buf[ctx_bufpos++] = input[i];
		}
	}

	/* Finalize */
	function finalize(outlen) { /* Result is in ctx_state */
		ctx_posl = (ctx_posl + ctx_bufpos) & 0xFFFFFFFF; /* Mark last block offset */
		if (ctx_posl < ctx_bufpos)
			ctx_posh = (ctx_posh + 1) & 0xFFFFFFFF;

		while (ctx_bufpos < 64) /* Zero pad what's left in the buffer */
			ctx_buf[ctx_bufpos++] = 0;
		compress(); /* Final block detected because ctx_c != 0 */
	}

	/* Do the magic */
	if (outlen == undefined)
		outlen = 32;
	init(new ByteArray(key), outlen);
	update(new ByteArray(msg));
	finalize(outlen);

	/* Produce the final hash value (big-endian) */
	var hash = new ByteArray();
	hash.ints = ctx_state;
	for (var i = 0; i < outlen; ++i)
		hash.push((hash.ints[Math.floor(i / 4)] >>> 8 * (i % 4)) & 0xFF);
	return hash;
}
