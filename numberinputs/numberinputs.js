numberinputs(document);

function numberinputs(e) {
	var inputs = e.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; ++i) {
		if (inputs[i].getAttribute("type") == "number")
			numberinput(inputs[i]);
	}
}

function numberinput(e) {
	var n = document.createElement("div");
	n.className += " numberinput";
	n.stepsize = Number(e.getAttribute("stepsize"));
	if (isNaN(n.stepsize) || n.stepsize == 0) n.stepsize = 1;
	n.min = e.getAttribute("min");
	if (n.min == "" || n.min == undefined)
		n.min = null;
	else n.min = Number(n.min);
	n.max = e.getAttribute("max");
	if (n.max == "" || n.max == undefined)
		n.max = null;
	else n.max = Number(n.max);
	e.type = "text";
	var btn_up = document.createElement("button");
	var btn_dn = document.createElement("button");
	btn_up.className = "up";
	btn_up.tabindex = -1;
	btn_dn.className = "dn";
	//btn_up.innerHTML = "+";
	//btn_dn.innerHTML = "-";
	btn_up.tabIndex = -1;
	btn_dn.tabIndex = -1;

	/* IE6/7 fires only ondblclick on double clicks, not much we can do about that it seems */
	btn_up.onmousedown = function(e) {
		var el = this.parentNode.getElementsByTagName("input")[0];
		ni_increment(el);
		if (el.ni_timeout == undefined) {
			el.ni_timeout = setTimeout(function() {
				el.ni_interval = setInterval(function() {
					ni_increment(el);
				}, 50);
			}, 500);
		}
	}

	btn_dn.onmousedown = function(e) {
		var el = this.parentNode.getElementsByTagName("input")[0];
		ni_decrement(el);
		if (el.ni_timeout == undefined) {
			el.ni_timeout = setTimeout(function() {
				el.ni_interval = setInterval(function() {
					ni_decrement(el);
				}, 50);
			}, 500);
		}
	}

	btn_up.onmouseup = btn_up.onmouseout = btn_dn.onmouseup = btn_dn.onmouseout = function() {
		var el = this.parentNode.getElementsByTagName("input")[0];
		clearTimeout(el.ni_timeout);
		clearInterval(el.ni_interval);
		el.ni_timeout = undefined;
	}

	e.onkeydown = function(e) {
		var e = window.event || e; // old IE support
		if (e.keyCode == 38) { /* arrowup */
			e.preventDefault ? e.preventDefault() : (e.returnValue = false);
			ni_increment(this.parentNode.getElementsByTagName("input")[0]);
		}
		if (e.keyCode == 40) { /* arrowdn */
			e.preventDefault ? e.preventDefault() : (e.returnValue = false);
			ni_decrement(this.parentNode.getElementsByTagName("input")[0]);
		}
	}

	e.parentNode.replaceChild(n, e);
	n.appendChild(e);
	n.appendChild(btn_up);
	n.appendChild(btn_dn);
	if (n.addEventListener) {
		// IE9, Chrome, Safari, Opera
		n.addEventListener("mousewheel", ni_wheel, false);
		// Firefox
		n.addEventListener("DOMMouseScroll", ni_wheel, false);
	}
	// IE 6/7/8
	else n.attachEvent("onmousewheel", ni_wheel);

	function ni_increment(el) {
		var n = Number(el.value);
		if (isNaN(n)) n = 0;
		var nv = Math.round((n + el.parentNode.stepsize) * 1e12) / 1e12;
		if (el.parentNode.max != null && nv > el.parentNode.max)
			nv = el.parentNode.max;
		el.value = nv;
		el.onchange();
	}

	function ni_decrement(el) {
		var n = Number(el.value);
		if (isNaN(n)) n = 0;
		var nv = Math.round((n - el.parentNode.stepsize) * 1e12) / 1e12;
		if (el.parentNode.min != null && nv < el.parentNode.min)
			nv = el.parentNode.min;
		el.value = nv;
		el.onchange();
	}

	function ni_wheel(e) {
		var e = window.event || e; // old IE support
		e.preventDefault ? e.preventDefault() : (e.returnValue = false);
		var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
		var el = e.srcElement || e.target;
		if (delta > 0)
			ni_increment(el);
		else ni_decrement(el);
	}
}
