/* sha256()
 *
 * SHA-256 hash function in javascript using ByteArray class.
 *
 * Arguments:
 *   msg: The message to be hashed.
 *     Can be a string or an array of bytes, strings and byte arrays (they'll be concatenated).
 *
 * Return value:
 *   Array of bytes, .toString() or .toString("hex") will make them into hex hash.
 *     For base64 or base64url encoding you can use .toString("base64") and .toString("base64url") respectively.
 *     The base64url format comes without trailing "=".
 *   Also the arrays will have .ints, which is an array storing the hash as 8 32-bit integers.
 *
 */
function sha256(msg) {
	/* Initialize hash values */
	/* (first 32 bits of the fractional parts of the square roots of the first 8 primes 2..19) */
	var h0 = 0x6a09e667, h1 = 0xbb67ae85, h2 = 0x3c6ef372, h3 = 0xa54ff53a,
	    h4 = 0x510e527f, h5 = 0x9b05688c, h6 = 0x1f83d9ab, h7 = 0x5be0cd19;

	/* Initialize array of round constants */
	/* (first 32 bits of the fractional parts of the cube roots of the first 64 primes 2..311) */
	var k = [
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
	];

	/* Deal with argument types */
	var arr = new ByteArray(msg);

	/* Pre-processing */
	var len = arr.length;
	var lenb = len * 8; /* Length in bits */
	arr.push(0x80); /* Add padding */
	while (arr.length % 64 != 56)
		arr.push(0);
	arr = arr.concat([rsh(lenb, 56) & 0xFF, rsh(lenb, 48) & 0xFF, rsh(lenb, 40) & 0xFF, rsh(lenb, 32) & 0xFF,
	                  (lenb >>> 24) & 0xFF, (lenb >>> 16) & 0xFF, (lenb >>> 8)  & 0xFF, (lenb >>> 0)  & 0xFF]);

	/* Process the message in successive 512-bit chunks */
	for (var off = 0; off < arr.length; off += 64) {
		/* Fill w with the first 16 32bit numbers */
		var w = [];
		for (var i = off; i < off + 64; i += 4)
			w[(i - off) / 4] = ((arr[i + 0] << 24) | (arr[i + 1] << 16) | (arr[i + 2] << 8) | (arr[i + 3] << 0)) >>> 0;

		/* Extend the first 16 words into the remaining 48 words w[16..63] of the message schedule array */
		for (var i = 16; i < 64; ++i) {
			var s0 = ror(w[i - 15], 7) ^ ror(w[i - 15], 18) ^ (w[i - 15] >>> 3);
			var s1 = ror(w[i - 2], 17) ^ ror(w[i - 2], 19) ^ (w[i - 2] >>> 10);
			w[i] = (w[i - 16] + s0 + w[i - 7] + s1) >>> 0;
		}

		/* Initialize working variables to current hash value */
		var a = h0, b = h1, c = h2, d = h3, e = h4, f = h5, g = h6, h = h7;

		/* Compression function main loop */
		for (var i = 0; i < 64; ++i) {
			var S1 = ror(e, 6) ^ ror(e, 11) ^ ror(e, 25);
			var ch = (e & f) ^ (~e & g);
			var temp1 = (h + S1 + ch + k[i] + w[i]) >>> 0;
			var S0 = ror(a, 2) ^ ror(a, 13) ^ ror(a, 22);
			var maj = (a & b) ^ (a & c) ^ (b & c);
			var temp2 = (S0 + maj) >>> 0;

			h = g;
			g = f;
			f = e;
			e = (d + temp1) >>> 0;
			d = c;
			c = b;
			b = a;
			a = (temp1 + temp2) >>> 0;
		}

		/* Add the compressed chunk to the current hash value */
		h0 = (h0 + a) >>> 0;
		h1 = (h1 + b) >>> 0;
		h2 = (h2 + c) >>> 0;
		h3 = (h3 + d) >>> 0;
		h4 = (h4 + e) >>> 0;
		h5 = (h5 + f) >>> 0;
		h6 = (h6 + g) >>> 0;
		h7 = (h7 + h) >>> 0;
	}

	/* Produce the final hash value (big-endian) */
	var hash = new ByteArray();
	hash.ints = [ h0, h1, h2, h3, h4, h5, h6, h7 ];
	for (var i = 0; i < 32; ++i)
		hash.push((hash.ints[Math.floor(i / 4)] >>> 24 - 8 * (i % 4)) & 0xFF);
	return hash;

	function rsh(a, b) { /* Important for shifting right by 32 bits or more */
		return Math.floor(a / Math.pow(2, b)) >>> 0;
	}

	function ror(a, b) {
		return rsh(a, b) | (a << (32 - b));
	}
}
